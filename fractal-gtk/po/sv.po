# Swedish translation for fractal.
# Copyright © 2018, 2019 fractal's COPYRIGHT HOLDER
# This file is distributed under the same license as the fractal package.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: fractal master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/fractal/issues\n"
"POT-Creation-Date: 2019-03-07 08:40+0000\n"
"PO-Revision-Date: 2019-03-13 10:58+0100\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.1\n"

#: fractal-gtk/res/gtk/help-overlay.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Allmänt"

#: fractal-gtk/res/gtk/help-overlay.ui:18
msgctxt "shortcut window"
msgid "Close the active room"
msgstr "Stäng det aktiva rummet"

#: fractal-gtk/res/gtk/help-overlay.ui:25
msgctxt "shortcut window"
msgid "Open / close the room sidebar search"
msgstr "Öppna / stäng rummets sidopanelssökning"

#: fractal-gtk/res/gtk/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr "Tangentbordsgenvägar"

#: fractal-gtk/res/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Quit"
msgstr "Avsluta"

#: fractal-gtk/res/gtk/help-overlay.ui:47
msgctxt "shortcut message"
msgid "Composing a new message"
msgstr "Skriva ett nytt meddelande"

#: fractal-gtk/res/gtk/help-overlay.ui:52
msgctxt "shortcut message"
msgid "Write on a new line"
msgstr "Skriv på en ny rad"

#: fractal-gtk/res/org.gnome.Fractal.appdata.xml.in.in:4
#: fractal-gtk/res/org.gnome.Fractal.desktop.in.in:3
msgid "Fractal"
msgstr "Fractal"

#: fractal-gtk/res/org.gnome.Fractal.appdata.xml.in.in:7
msgid "Daniel García Moreno"
msgstr "Daniel García Moreno"

#: fractal-gtk/res/org.gnome.Fractal.appdata.xml.in.in:8
msgid "Matrix group messaging app"
msgstr "Matrix-gruppmeddelandeprogram"

#: fractal-gtk/res/org.gnome.Fractal.appdata.xml.in.in:10
msgid ""
"Fractal is a Matrix messaging app for GNOME written in Rust. Its interface "
"is optimized for collaboration in large groups, such as free software "
"projects."
msgstr ""
"Fractal är ett Matrix-meddelandeprogram för GNOME skrivet i Rust. Dess "
"gränssnitt är optimerat för samarbete i stora grupper, så som projekt inom "
"fri programvara."

#: fractal-gtk/res/org.gnome.Fractal.desktop.in.in:4
msgid ""
"Fractal is a decentralized, secure messaging client for collaborative group "
"communication."
msgstr ""
"Fractal är en decentraliserad, säker meddelandeklient för samarbetande "
"gruppkommunikation."

#: fractal-gtk/res/org.gnome.Fractal.desktop.in.in:5
msgid "Fractal group messaging"
msgstr "Fractal gruppmeddelanden"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: fractal-gtk/res/org.gnome.Fractal.desktop.in.in:8
msgid "@icon@"
msgstr "@icon@"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: fractal-gtk/res/org.gnome.Fractal.desktop.in.in:14
msgid "Matrix;matrix.org;chat;irc;communications;talk;riot;"
msgstr "Matrix;matrix.org;chatt;irc;kommunikation;samtal;riot;"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:13
msgid "Type of password and token storage"
msgstr "Typ av lösenords- och åtkomstelementlagring"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:14
msgid "Type of password and token storage, default value is: Secret Service"
msgstr ""
"Typ av lösenords- och åtkomstelementlagring, standardvärde är: Secret Service"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:21
msgid "If markdown sending is active"
msgstr "Om sändning av markdown är aktiv"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:22
msgid "Whether support for sending markdown messages is on"
msgstr "Huruvida stöd för sändning av markdown-meddelanden är påslaget"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:29
msgid "X position of the main window on startup"
msgstr "X-position för huvudfönstret vid uppstart"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:34
msgid "Y position of the main window on startup"
msgstr "Y-position för huvudfönstret vid uppstart"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:39
msgid "Width of the main window on startup"
msgstr "Huvudfönstrets bredd vid uppstart"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:44
msgid "Height of the main window on startup"
msgstr "Huvudfönstrets höjd vid uppstart"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:49
msgid "Whether the main window is maximized on startup"
msgstr "Huruvida huvudfönstret är maximerat vid uppstart"

#: fractal-gtk/res/ui/account_settings.ui:71
msgid "Other people can find you by searching for any of these identifiers."
msgstr "Andra kan hitta dig genom att söka efter någon av dessa identifierare."

#: fractal-gtk/res/ui/account_settings.ui:99
msgid "Name"
msgstr "Namn"

#: fractal-gtk/res/ui/account_settings.ui:122
msgid "Type in your name"
msgstr "Mata in ditt namn"

#: fractal-gtk/res/ui/account_settings.ui:151
msgid "Email"
msgstr "E-post"

#: fractal-gtk/res/ui/account_settings.ui:180
msgid "Phone"
msgstr "Telefon"

#: fractal-gtk/res/ui/account_settings.ui:209
#: fractal-gtk/res/ui/main_window.ui:463
msgid "Password"
msgstr "Lösenord"

#: fractal-gtk/res/ui/account_settings.ui:283
msgid "Advanced Information"
msgstr "Avancerad information"

#: fractal-gtk/res/ui/account_settings.ui:338
msgid "Homeserver"
msgstr "Hemserver"

#: fractal-gtk/res/ui/account_settings.ui:353
#: fractal-gtk/res/ui/account_settings.ui:381
#: fractal-gtk/res/ui/account_settings.ui:409
msgid "None"
msgstr "Ingen"

#: fractal-gtk/res/ui/account_settings.ui:366
msgid "Matrix ID"
msgstr "Matrix-ID"

#: fractal-gtk/res/ui/account_settings.ui:394
msgid "Device ID"
msgstr "Enhets-ID"

#: fractal-gtk/res/ui/account_settings.ui:463
#: fractal-gtk/res/ui/account_settings.ui:569
msgid "Deactivate Account"
msgstr "Inaktivera konto"

#: fractal-gtk/res/ui/account_settings.ui:515
msgid ""
"Deactivating your account means that you will lose all your messages, "
"contacts, and files."
msgstr ""
"Att inaktivera ditt konto betyder att du kommer att förlora alla dina "
"meddelanden, kontakter och filer."

#: fractal-gtk/res/ui/account_settings.ui:530
msgid ""
"To confirm that you really want to deactivate this account type in your "
"password:"
msgstr ""
"Mata in ditt lösenord för att bekräfta att du verkligen vill inaktivera "
"detta konto:"

#: fractal-gtk/res/ui/account_settings.ui:555
msgid "Also erase all messages"
msgstr "Ta även bort alla meddelanden"

#: fractal-gtk/res/ui/account_settings.ui:661
#: fractal-gtk/res/ui/main_menu.ui:109
msgid "Account Settings"
msgstr "Kontoinställningar"

#: fractal-gtk/res/ui/account_settings.ui:677
#: fractal-gtk/res/ui/main_window.ui:870 fractal-gtk/res/ui/main_window.ui:1006
#: fractal-gtk/res/ui/main_window.ui:1074
#: fractal-gtk/res/ui/media_viewer.ui:250
#: fractal-gtk/res/ui/room_settings.ui:1034
msgid "Back"
msgstr "Bakåt"

#: fractal-gtk/res/ui/account_settings.ui:698
msgid "Check your email"
msgstr "Kontrollera din e-post"

#: fractal-gtk/res/ui/account_settings.ui:701
#: fractal-gtk/res/ui/direct_chat.ui:129 fractal-gtk/res/ui/invite_user.ui:129
#: fractal-gtk/res/ui/join_room.ui:93 fractal-gtk/res/ui/leave_room.ui:32
#: fractal-gtk/res/ui/new_room.ui:42 fractal-gtk/src/appop/account.rs:69
#: fractal-gtk/src/appop/account.rs:128 fractal-gtk/src/appop/attach.rs:58
msgid "Cancel"
msgstr "Avbryt"

#: fractal-gtk/res/ui/account_settings.ui:709
msgid "Apply"
msgstr "Verkställ"

#: fractal-gtk/res/ui/add_room_menu.ui:22
msgid "Room Directory"
msgstr "Rumskatalog"

#: fractal-gtk/res/ui/add_room_menu.ui:36
msgid "Join Room"
msgstr "Gå in i rum"

#: fractal-gtk/res/ui/add_room_menu.ui:63
msgid "New Room"
msgstr "Nytt rum"

#: fractal-gtk/res/ui/add_room_menu.ui:77
msgid "New Direct Chat"
msgstr "Ny direktchatt"

#: fractal-gtk/res/ui/audio_player.ui:34 fractal-gtk/res/ui/audio_player.ui:50
msgid "Play"
msgstr "Spela upp"

#: fractal-gtk/res/ui/direct_chat.ui:126
msgid "New direct chat"
msgstr "Ny direktchatt"

#: fractal-gtk/res/ui/direct_chat.ui:137
msgid "Start chat"
msgstr "Starta chatt"

#: fractal-gtk/res/ui/filechooser.ui:61
msgid "Select room image file"
msgstr "Välj bildfil för rum"

#: fractal-gtk/res/ui/invite.ui:18
msgid "Invitation"
msgstr "Inbjudan"

# TODO: Are quotes translatable? How is the name obtained in code?
#: fractal-gtk/res/ui/invite.ui:19
msgid "You’ve been invited to join \"\", you can accept or reject"
msgstr "Du har bjudits in att gå in i \"\", du kan acceptera eller avvisa"

#: fractal-gtk/res/ui/invite.ui:32
msgid "Reject"
msgstr "Avvisa"

#: fractal-gtk/res/ui/invite.ui:45
msgid "Accept"
msgstr "Acceptera"

#: fractal-gtk/res/ui/invite_user.ui:126 fractal-gtk/res/ui/invite_user.ui:137
#: fractal-gtk/src/appop/invite.rs:146
msgid "Invite"
msgstr "Bjud in"

#: fractal-gtk/res/ui/join_room.ui:52
msgid "ID or Alias"
msgstr "ID eller alias"

#: fractal-gtk/res/ui/join_room.ui:90
msgid "Join room"
msgstr "Gå in i rum"

#: fractal-gtk/res/ui/join_room.ui:101 fractal-gtk/src/widgets/room.rs:117
msgid "Join"
msgstr "Gå in"

#: fractal-gtk/res/ui/leave_room.ui:18
msgid "Leave?"
msgstr "Lämna?"

#: fractal-gtk/res/ui/leave_room.ui:19
msgid ""
"Once you leave, you won’t be able to interact with people in the room "
"anymore."
msgstr ""
"Då du lämnar kommer du inte längre kunna interagera med personer i rummet."

#: fractal-gtk/res/ui/leave_room.ui:45
msgid "Leave room"
msgstr "Lämna rum"

#: fractal-gtk/res/ui/main_menu.ui:123
msgid "Log Out"
msgstr "Logga ut"

#: fractal-gtk/res/ui/main_menu.ui:150
msgid "Keyboard _Shortcuts"
msgstr "_Tangentbordsgenvägar"

#: fractal-gtk/res/ui/main_menu.ui:164
msgid "_About Fractal"
msgstr "_Om Fractal"

#: fractal-gtk/res/ui/main_window.ui:183
msgid "No room selected"
msgstr "Inget rum markerat"

#: fractal-gtk/res/ui/main_window.ui:198
msgid "Join a room to start chatting"
msgstr "Gå in i ett rum för att börja chatta"

#: fractal-gtk/res/ui/main_window.ui:208
msgid "No room"
msgstr "Inget rum"

#: fractal-gtk/res/ui/main_window.ui:289
msgid "Chat"
msgstr "Chatt"

#: fractal-gtk/res/ui/main_window.ui:341
msgid "Directory"
msgstr "Katalog"

#: fractal-gtk/res/ui/main_window.ui:365
msgid "Loading"
msgstr "Läser in"

#: fractal-gtk/res/ui/main_window.ui:391
msgctxt "big label"
msgid "Log In"
msgstr "Logga in"

#: fractal-gtk/res/ui/main_window.ui:413
msgid "Username"
msgstr "Användarnamn"

#: fractal-gtk/res/ui/main_window.ui:430
msgid "Matrix username, email or phone number"
msgstr "Matrix-användarnamn, e-post eller telefonnummer"

#: fractal-gtk/res/ui/main_window.ui:443
msgctxt "login button"
msgid "Log In"
msgstr "Logga in"

#: fractal-gtk/res/ui/main_window.ui:508
msgid "Home server URL"
msgstr "URL för hemserver"

#: fractal-gtk/res/ui/main_window.ui:526
msgid "Identity server URL"
msgstr "URL för identitetsserver"

#: fractal-gtk/res/ui/main_window.ui:543
msgid "Matrix Server"
msgstr "Matrix-server"

#: fractal-gtk/res/ui/main_window.ui:562
msgid "Identity server"
msgstr "Identitetsserver"

#: fractal-gtk/res/ui/main_window.ui:599
msgid "Advanced"
msgstr "Avancerat"

#: fractal-gtk/res/ui/main_window.ui:614 fractal-gtk/src/appop/login.rs:138
msgid "Invalid username or password"
msgstr "Ogiltigt användarnamn eller lösenord"

#: fractal-gtk/res/ui/main_window.ui:668
msgid "Reset Password"
msgstr "Nollställ lösenord"

#: fractal-gtk/res/ui/main_window.ui:682
msgid "Create Account"
msgstr "Skapa konto"

#: fractal-gtk/res/ui/main_window.ui:696
msgid "Log In as Guest"
msgstr "Logga in som gäst"

#: fractal-gtk/res/ui/main_window.ui:718
msgid "Login"
msgstr "Inloggning"

#: fractal-gtk/res/ui/main_window.ui:763
msgid "User"
msgstr "Användare"

#: fractal-gtk/res/ui/main_window.ui:790
msgid "Add"
msgstr "Lägg till"

#: fractal-gtk/res/ui/main_window.ui:820
msgid "Room search"
msgstr "Rumssökning"

#: fractal-gtk/res/ui/main_window.ui:894 fractal-gtk/res/ui/new_room.ui:108
msgid "Room name"
msgstr "Rummets namn"

#: fractal-gtk/res/ui/main_window.ui:909
msgid "Room topic"
msgstr "Rummets ämne"

#: fractal-gtk/res/ui/main_window.ui:942
msgid "Room Menu"
msgstr "Rumsmeny"

#: fractal-gtk/res/ui/main_window.ui:1036
#: fractal-gtk/res/ui/server_chooser_menu.ui:65
#: fractal-gtk/src/app/connect/directory.rs:127
msgid "Default Matrix Server"
msgstr "Standard-Matrix-server"

#: fractal-gtk/res/ui/markdown_popover.ui:23
msgid "Markdown"
msgstr "Markdown"

#: fractal-gtk/res/ui/markdown_popover.ui:67
msgid "> quote"
msgstr "> citat"

#: fractal-gtk/res/ui/markdown_popover.ui:78
msgid "**bold**"
msgstr "**fet**"

#: fractal-gtk/res/ui/markdown_popover.ui:93
msgid "`code`"
msgstr "`kod`"

#: fractal-gtk/res/ui/markdown_popover.ui:105
msgid "*italic*"
msgstr "*kursiv*"

#: fractal-gtk/res/ui/media_viewer.ui:185
msgid "Loading more media"
msgstr "Läser in mer media"

#: fractal-gtk/res/ui/media_viewer.ui:233
msgid "Media viewer"
msgstr "Mediavisare"

#: fractal-gtk/res/ui/media_viewer_menu.ui:26
msgid "Save as…"
msgstr "Spara som…"

#: fractal-gtk/res/ui/members.ui:25 fractal-gtk/res/ui/room_settings.ui:940
msgid "Search for room members"
msgstr "Sök efter rumsmedlemmar"

#: fractal-gtk/res/ui/message_menu.ui:22
msgid "Reply"
msgstr "Svara"

#: fractal-gtk/res/ui/message_menu.ui:34
msgid "Open With…"
msgstr "Öppna med…"

#: fractal-gtk/res/ui/message_menu.ui:47
msgid "Save Image As…"
msgstr "Spara bild som…"

#: fractal-gtk/res/ui/message_menu.ui:60
msgid "Copy Image"
msgstr "Kopiera bild"

#: fractal-gtk/res/ui/message_menu.ui:73
msgid "Copy Selection"
msgstr "Kopiera markering"

#: fractal-gtk/res/ui/message_menu.ui:86
msgid "Copy Text"
msgstr "Kopiera text"

#: fractal-gtk/res/ui/message_menu.ui:101
msgid "View Source"
msgstr "Visa källa"

#: fractal-gtk/res/ui/message_menu.ui:125
msgid "Delete Message"
msgstr "Ta bort meddelande"

#: fractal-gtk/res/ui/msg_src_window.ui:21
msgid "Message Source"
msgstr "Meddelandekälla"

#: fractal-gtk/res/ui/msg_src_window.ui:24
msgid "Copy To Clipboard"
msgstr "Kopiera till urklipp"

#: fractal-gtk/res/ui/msg_src_window.ui:32
msgid "Close"
msgstr "Stäng"

#: fractal-gtk/res/ui/new_room.ui:14
msgid "Private Chat"
msgstr "Privat chatt"

#: fractal-gtk/res/ui/new_room.ui:18 fractal-gtk/res/ui/new_room.ui:187
msgid "Public"
msgstr "Öppen"

#: fractal-gtk/res/ui/new_room.ui:39
msgid "Create new room"
msgstr "Skapa nytt rum"

#: fractal-gtk/res/ui/new_room.ui:50
msgid "Create"
msgstr "Skapa"

#: fractal-gtk/res/ui/new_room.ui:152
msgid "Visibility"
msgstr "Synlighet"

#: fractal-gtk/res/ui/new_room.ui:171
msgid "Private"
msgstr "Privat"

#: fractal-gtk/res/ui/password_dialog.ui:22
#: fractal-gtk/src/widgets/file_dialog.rs:13
#: fractal-gtk/src/widgets/file_dialog.rs:34
#: fractal-gtk/src/widgets/media_viewer.rs:790
msgid "_Cancel"
msgstr "_Avbryt"

#: fractal-gtk/res/ui/password_dialog.ui:35
msgid "Ch_ange"
msgstr "Än_dra"

#: fractal-gtk/res/ui/password_dialog.ui:133
msgid "The passwords do not match."
msgstr "Lösenorden stämmer inte överens."

#: fractal-gtk/res/ui/password_dialog.ui:151
msgid "_Verify New Password"
msgstr "_Verifiera nytt lösenord"

#: fractal-gtk/res/ui/password_dialog.ui:168
msgid "_New Password"
msgstr "_Nytt lösenord"

#: fractal-gtk/res/ui/password_dialog.ui:212
msgid "Current _Password"
msgstr "Aktuellt _lösenord"

#: fractal-gtk/res/ui/room_menu.ui:22
msgid "Room Details"
msgstr "Rumsdetaljer"

#: fractal-gtk/res/ui/room_menu.ui:36
msgid "Invite to This Room"
msgstr "Bjud in till detta rum"

#: fractal-gtk/res/ui/room_menu.ui:63
msgid "Leave Room"
msgstr "Lämna rum"

#: fractal-gtk/res/ui/room_settings.ui:92
msgid "Unknown"
msgstr "Okänt"

#: fractal-gtk/res/ui/room_settings.ui:118
msgid "Add name"
msgstr "Lägg till namn"

#: fractal-gtk/res/ui/room_settings.ui:150
msgid "Add topic"
msgstr "Lägg till ämne"

#: fractal-gtk/res/ui/room_settings.ui:174
msgid "Type in your room topic"
msgstr "Skriv in ditt ämne för rummet"

#: fractal-gtk/res/ui/room_settings.ui:202
msgid "No room description"
msgstr "Ingen rumsbeskrivning"

#: fractal-gtk/res/ui/room_settings.ui:239
msgid "Notifications"
msgstr "Aviseringar"

#: fractal-gtk/res/ui/room_settings.ui:280
msgid "Notification sounds"
msgstr "Aviseringsljud"

#: fractal-gtk/res/ui/room_settings.ui:325
msgid "For all messages"
msgstr "För alla meddelanden"

#: fractal-gtk/res/ui/room_settings.ui:366
msgid "Only for mentions"
msgstr "Endast för omnämnanden"

#: fractal-gtk/res/ui/room_settings.ui:397
msgid "Shared Media"
msgstr "Delade media"

#: fractal-gtk/res/ui/room_settings.ui:444
msgid "photos"
msgstr "foton"

#: fractal-gtk/res/ui/room_settings.ui:484
msgid "videos"
msgstr "videoklipp"

#: fractal-gtk/res/ui/room_settings.ui:524
msgid "documents"
msgstr "dokument"

#: fractal-gtk/res/ui/room_settings.ui:555
msgid "New members can see"
msgstr "Nya medlemmar kan se"

#: fractal-gtk/res/ui/room_settings.ui:600
msgid "All room history"
msgstr "All rumshistorik"

#: fractal-gtk/res/ui/room_settings.ui:641
msgid "History after they were invited"
msgstr "Historik efter att de bjöds in"

#: fractal-gtk/res/ui/room_settings.ui:672
msgid "Room Visibility"
msgstr "Synlighet för rum"

#: fractal-gtk/res/ui/room_settings.ui:719
msgid "Allow guests"
msgstr "Tillåt gäster"

#: fractal-gtk/res/ui/room_settings.ui:759
msgid "Allow joining without invite"
msgstr "Tillåt ingång utan inbjudan"

#: fractal-gtk/res/ui/room_settings.ui:799
msgid "Publish in room directory"
msgstr "Publicera i rumskatalog"

#: fractal-gtk/res/ui/room_settings.ui:830
msgid "Join addresses"
msgstr "Ingångsadresser"

#: fractal-gtk/res/ui/room_settings.ui:889
msgid "members"
msgstr "medlemmar"

#: fractal-gtk/res/ui/room_settings.ui:905
msgid "Invite New Member"
msgstr "Bjud in ny medlem"

#: fractal-gtk/res/ui/room_settings.ui:1018
msgid "Details"
msgstr "Detaljer"

#: fractal-gtk/res/ui/scroll_widget.ui:63
msgid "Scroll to bottom"
msgstr "Rulla längst ner"

#: fractal-gtk/res/ui/server_chooser_menu.ui:34
msgid "Show rooms from:"
msgstr "Visa rum från:"

#: fractal-gtk/res/ui/server_chooser_menu.ui:90
msgid "Your homeserver"
msgstr "Din hemserver"

#: fractal-gtk/res/ui/server_chooser_menu.ui:134
msgid "Other Protocol"
msgstr "Annat protokoll"

#: fractal-gtk/res/ui/server_chooser_menu.ui:207
msgid "Other Homeserver"
msgstr "Annan hemserver"

#: fractal-gtk/res/ui/server_chooser_menu.ui:235
msgid "Homeserver URL"
msgstr "URL för hemserver"

#: fractal-gtk/src/actions/account_settings.rs:31
#: fractal-gtk/src/actions/room_settings.rs:35
msgid "Images"
msgstr "Bilder"

#: fractal-gtk/src/actions/account_settings.rs:32
#: fractal-gtk/src/actions/room_settings.rs:37
msgid "Select a new avatar"
msgstr "Välj en ny profilbild"

#: fractal-gtk/src/actions/account_settings.rs:37
#: fractal-gtk/src/actions/room_settings.rs:42
msgid "Couldn’t open file"
msgstr "Det gick inte att öppna fil"

#: fractal-gtk/src/actions/global.rs:220
msgid "Select a file"
msgstr "Välj en fil"

#: fractal-gtk/src/actions/room_history.rs:58
msgid "This message has no source."
msgstr "Det här meddelandet har ingen källa."

#: fractal-gtk/src/actions/room_history.rs:110
#: fractal-gtk/src/actions/room_history.rs:142
msgid "Could not download the file"
msgstr "Kunde inte hämta filen"

#: fractal-gtk/src/actions/room_history.rs:120
msgid "Couldn’t save file"
msgstr "Det gick inte att spara fil"

#: fractal-gtk/src/app/backend_loop.rs:65
msgid "Email is already in use"
msgstr "E-postadressen används redan"

#: fractal-gtk/src/app/backend_loop.rs:69
msgid "Phone number is already in use"
msgstr "Telefonnumret används redan"

#: fractal-gtk/src/app/backend_loop.rs:203
msgid "Couldn’t delete the account"
msgstr "Kunde inte ta bort kontot"

#: fractal-gtk/src/app/backend_loop.rs:208
msgid "Couldn’t change the password"
msgstr "Kunde inte ändra lösenordet"

#: fractal-gtk/src/app/backend_loop.rs:213
msgid "Couldn’t add the email address."
msgstr "Kunde inte lägga till e-postadressen."

#: fractal-gtk/src/app/backend_loop.rs:218
msgid "Couldn’t add the phone number."
msgstr "Kunde inte lägga till telefonnumret."

#: fractal-gtk/src/app/backend_loop.rs:225
msgid "Can’t create the room, try again"
msgstr "Det går inte att skapa rummet, försök igen"

#: fractal-gtk/src/app/backend_loop.rs:233
msgid "Can’t join the room, try again."
msgstr "Det går inte att gå in i rummet, försök igen."

#: fractal-gtk/src/app/backend_loop.rs:239
msgid "Can’t login, try again"
msgstr "Det går inte att logga in, försök igen"

#: fractal-gtk/src/app/backend_loop.rs:254
msgid "Error sending message"
msgstr "Fel vid sändning av meddelande"

#: fractal-gtk/src/app/backend_loop.rs:259
msgid "Error deleting message"
msgstr "Fel vid borttagning av meddelande"

#: fractal-gtk/src/app/backend_loop.rs:263
msgid "Error searching for rooms"
msgstr "Fel vid sökning efter rum"

#: fractal-gtk/src/appop/about.rs:24
msgid "A Matrix.org client for GNOME"
msgstr "En Matrix.org-klient för GNOME"

#: fractal-gtk/src/appop/about.rs:25
msgid "© 2017–2018 Daniel García Moreno, et al."
msgstr "© 2017–2018 Daniel García Moreno med flera."

#: fractal-gtk/src/appop/about.rs:31
msgid "Learn more about Fractal"
msgstr "Lär dig mer om Fractal"

#: fractal-gtk/src/appop/about.rs:32
msgid "translator-credits"
msgstr ""
"Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"\n"
"Skicka synpunkter på översättningen till\n"
"<tp-sv@listor.tp-sv.se>."

#: fractal-gtk/src/appop/about.rs:47
msgid "Name by"
msgstr "Namn av"

#: fractal-gtk/src/appop/account.rs:41
msgid "The validation code is not correct."
msgstr "Valideringskoden är felaktig."

#: fractal-gtk/src/appop/account.rs:54
msgid "Enter the code received via SMS"
msgstr "Ange koden du mottagit via SMS"

#: fractal-gtk/src/appop/account.rs:70 fractal-gtk/src/appop/account.rs:129
msgid "Continue"
msgstr "Fortsätt"

#: fractal-gtk/src/appop/account.rs:117
msgid ""
"In order to add this email address, go to your inbox and follow the link you "
"received. Once you’ve done that, click Continue."
msgstr ""
"För att lägga till denna e-postadress, gå till din inkorg och följ länken "
"som du tog emot. Klicka sedan Fortsätt då du gjort detta."

#: fractal-gtk/src/appop/account.rs:167
msgid "OK"
msgstr "OK"

#: fractal-gtk/src/appop/account.rs:694
msgid "Are you sure you want to delete your account?"
msgstr "Är du säker på att du vill ta bort ditt konto?"

#: fractal-gtk/src/appop/attach.rs:43
msgid "Image from Clipboard"
msgstr "Bild från urklipp"

#: fractal-gtk/src/appop/attach.rs:59
msgid "Send"
msgstr "Skicka"

#: fractal-gtk/src/appop/invite.rs:144
msgid "Invite to {name}"
msgstr "Bjud in till {name}"

#: fractal-gtk/src/appop/invite.rs:237
msgid "Join {room_name}?"
msgstr "Gå in i {room_name}?"

#: fractal-gtk/src/appop/invite.rs:242
msgid ""
"You’ve been invited to join <b>{room_name}</b> room by <b>{sender_name}</b>"
msgstr ""
"Du har bjudits in att gå in i rummet <b>{room_name}</b> av <b>{sender_name}</"
"b>"

#: fractal-gtk/src/appop/invite.rs:247
msgid "You’ve been invited to join <b>{room_name}</b>"
msgstr "Du har bjudits in att gå in i <b>{room_name}</b>"

#: fractal-gtk/src/appop/login.rs:142
msgid "Unknown Error"
msgstr "Okänt fel"

#: fractal-gtk/src/appop/login.rs:223
msgid "Passwords didn’t match, try again"
msgstr "Lösenorden överensstämde inte, försök igen"

#: fractal-gtk/src/appop/notify.rs:49
msgid " (direct message)"
msgstr " (direktmeddelande)"

#: fractal-gtk/src/appop/room.rs:217
msgid "Leave {room_name}?"
msgstr "Lämna {room_name}?"

#: fractal-gtk/src/appop/room.rs:440
msgid "EMPTY ROOM"
msgstr "TOMT RUM"

#: fractal-gtk/src/appop/room.rs:442
msgid "{m1} and {m2}"
msgstr "{m1} och {m2}"

#: fractal-gtk/src/appop/room.rs:443
msgid "{m1} and Others"
msgstr "{m1} med flera"

#: fractal-gtk/src/appop/room.rs:539
msgid "Several users are typing…"
msgstr "Flera användare skriver…"

#: fractal-gtk/src/appop/room.rs:542
msgid "<b>{}</b> is typing…"
msgid_plural "<b>{}</b> and {} are typing…"
msgstr[0] "<b>{}</b> skriver…"
msgstr[1] "<b>{}</b> och {} skriver…"

#: fractal-gtk/src/appop/sync.rs:10
msgid "Syncing, this could take a while"
msgstr "Synkroniserar, detta kan ta ett tag"

#: fractal-gtk/src/widgets/file_dialog.rs:9
#: fractal-gtk/src/widgets/media_viewer.rs:786
msgid "Save media as"
msgstr "Spara media som"

#: fractal-gtk/src/widgets/file_dialog.rs:12
#: fractal-gtk/src/widgets/media_viewer.rs:789
msgid "_Save"
msgstr "_Spara"

#: fractal-gtk/src/widgets/file_dialog.rs:33
msgid "_Select"
msgstr "_Välj"

#: fractal-gtk/src/widgets/media_viewer.rs:746
msgid "Error while loading previous media"
msgstr "Fel vid inläsning av tidigare media"

#: fractal-gtk/src/widgets/media_viewer.rs:799
msgid "Could not save the file"
msgstr "Det gick inte att spara filen"

#: fractal-gtk/src/widgets/members_list.rs:42
msgid "No matching members found"
msgstr "Inga matchande medlemmar hittades"

#: fractal-gtk/src/widgets/message.rs:359
msgid "Could not retrieve file URI"
msgstr "Kunde inte erhålla fil-URI"

#: fractal-gtk/src/widgets/message.rs:375
#: fractal-gtk/src/widgets/message.rs:401
msgid "Save"
msgstr "Spara"

#: fractal-gtk/src/widgets/message.rs:409
msgid "Open"
msgstr "Öppna"

#. Use 12h time format (AM/PM)
#: fractal-gtk/src/widgets/message.rs:431
msgid "%l∶%M %p"
msgstr "%l∶%M %p"

#. Use 24 time format
#: fractal-gtk/src/widgets/message.rs:434
msgid "%R"
msgstr "%R"

#. Translators: This is a date format in the day divider without the year
#: fractal-gtk/src/widgets/room_history.rs:318
msgid "%B %e"
msgstr "%e %B"

#. Translators: This is a date format in the day divider with the year
#: fractal-gtk/src/widgets/room_history.rs:321
msgid "%B %e, %Y"
msgstr "%e %B, %Y"

#: fractal-gtk/src/widgets/room_history.rs:345
msgid "New Messages"
msgstr "Nya meddelanden"

#: fractal-gtk/src/widgets/room_settings.rs:196
msgid "Room · {} member"
msgid_plural "Room · {} members"
msgstr[0] "Rum · {} medlem"
msgstr[1] "Rum · {} medlemmar"

#: fractal-gtk/src/widgets/room_settings.rs:612
msgid "{} member"
msgid_plural "{} members"
msgstr[0] "{} medlem"
msgstr[1] "{} medlemmar"

#: fractal-gtk/src/widgets/roomlist.rs:424
msgid "Invites"
msgstr "Inbjudningar"

#: fractal-gtk/src/widgets/roomlist.rs:425
msgid "You don’t have any invitations"
msgstr "Du har inga inbjudningar"

#: fractal-gtk/src/widgets/roomlist.rs:429
msgid "Favorites"
msgstr "Favoriter"

#: fractal-gtk/src/widgets/roomlist.rs:430
msgid "Drag and drop rooms here to add them to your favorites"
msgstr "Dra och släpp rum här för att lägga till dem till dina favoriter"

#: fractal-gtk/src/widgets/roomlist.rs:434
msgid "Rooms"
msgstr "Rum"

#: fractal-gtk/src/widgets/roomlist.rs:435
msgid "You don’t have any rooms yet"
msgstr "Du har inga rum ännu"

#~ msgid "Pick a new room avatar"
#~ msgstr "Välj en ny profilbild för rum"

#~ msgid "_About"
#~ msgstr "_Om"

#~ msgid "_Quit"
#~ msgstr "A_vsluta"

#~ msgid "Attach files"
#~ msgstr "Bifoga filer"

#~ msgid "Text formatting"
#~ msgstr "Textformatering"

#~ msgid "This is an app-notification"
#~ msgstr "Detta är en programavisering"

#~ msgid "Stickers"
#~ msgstr "Dekaler"

#~ msgid "Do you want to overwrite the file?"
#~ msgstr "Vill du skriva över filen?"
